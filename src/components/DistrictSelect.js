import React, { useState } from "react";
import styled from "styled-components";

const DistrictSelect = props => {
  const data = props;
  const [activeRegion, setActiveRegion] = useState(null);
  const [activeDistrict, setActiveDistrict] = useState(null);

  const handleRegionChange = event => {
    setActiveRegion(event.target.value);
  };

  const handleDistrictChange = event => {
    setActiveDistrict(event.target.value);
  };

  return (
    <>
      <Wrapper>
        {data &&
          Object.keys(data.regions).map((item, index) => {
            return (
              <Item key={item}>
                <RadioButton
                  type="radio"
                  name="radio"
                  value={item}
                  onChange={event => handleRegionChange(event)}
                />
                <RadioButtonLabel />
                <div>{item}</div>
              </Item>
            );
          })}
      </Wrapper>

      {activeRegion && (
        <>
          <DistrictTitle>Vyberte okres:</DistrictTitle>
          <Wrapper>
            {activeRegion &&
              data.regions[activeRegion].map((item, index) => {
                return (
                  <Item key={item}>
                    <RadioButton
                      type="radio"
                      name="radio"
                      value={item}
                      onChange={event => handleDistrictChange(event)}
                    />
                    <RadioButtonLabel />
                    <div>{item}</div>
                  </Item>
                );
              })}
          </Wrapper>
        </>
      )}

      {activeDistrict && (
        <DistrictResult>Vybral jste si: {activeDistrict}</DistrictResult>
      )}
    </>
  );
};
export default DistrictSelect;

const Wrapper = styled.form`
  height: auto;
  width: 100%;
  padding: 0px 16px 24px;
  box-sizing: border-box;

  display: grid;
  grid-column-gap: 0.3rem;
  grid-template-columns: 1fr 1fr;
`;

const Item = styled.label`
  display: flex;
  align-items: center;
  height: 48px;
  position: relative;
`;

const RadioButtonLabel = styled.label`
  position: absolute;
  top: 25%;
  left: 4px;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  background: white;
  border: 1px solid #bebebe;
`;
const RadioButton = styled.input`
  opacity: 0;
  z-index: 1;
  border-radius: 50%;
  width: 24px;
  height: 24px;
  margin-right: 10px;
  &:hover ~ ${RadioButtonLabel} {
    background: #bebebe;
    &::after {
      content: "";
      display: block;
      border-radius: 50%;
      width: 12px;
      height: 12px;
      margin: 6px;
      background: #eeeeee;
    }
  }
  &:checked + ${RadioButtonLabel} {
    background: blue;
    border: 1px solid blue;
    &::after {
      content: "";
      display: block;
      border-radius: 50%;
      width: 12px;
      height: 12px;
      margin: 6px;
      box-shadow: 1px 3px 3px 1px rgba(0, 0, 0, 0.1);
      background: white;
    }
  }
`;

const DistrictTitle = styled.h3`
  padding: 0px 16px;
`;

const DistrictResult = styled.div`
  padding: 0px 16px;
`;
