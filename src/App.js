import React from "react";
import DistrictSelect from "./components/DistrictSelect";

const App = () => {
  const data = {
    Plzenský: [
      "Domažlice",
      "Klatovy",
      "Plzeň-jih",
      "Plzeň mesto",
      "Plzeň sever",
      "Rokycany",
      "Tachov"
    ],
    Olomoucký: ["olomoucky test 1", "olomoucky test 2", "olomoucky test 3"],
    Pardubický: ["pardubicky 1", "pardubicky 2", "pardubicky 3"],
    Moravskosliezsky: ["moravskoslezky 1", "moravskoslezsky 2"],
    Liberecký: ["liberecky 1", "liberecky 2", "liberecky 3", "liberecky 4"],
    Zlínsky: ["zlinsky 1", "zlinsky 2", "zlinsky 3"]
  };
  return (
    <div className="App">
      <header className="App-header">
        <DistrictSelect regions={data}></DistrictSelect>
      </header>
    </div>
  );
};

export default App;
